"use strict";

/**
 * This function will create a single table and populate it with the information contained in the char_freq_map map.
 * 
 * The table will have the given id and class name, and will be sorted according to the ordering of the given
 * sortedKeys array.
 */


function createTable(table, char_freq_map, sortedKeys) {

    var findTbody = table.find("tbody");
    findTbody.empty();
    for (var characters of sortedKeys) {
        $(findTbody).append("<tr><td>" + characters + "</td><td>" + char_freq_map[characters] + "</td></tr>")
    }

    // TODO step 3: implement this function. Details are in the assignment handout.
}

/**
 * This function will loop through all the characters in the supplied text, and use an associative
 * array to store info about the number of each letter that appears in the string. The info will be
 * stored in the char_freq_map.
 * 
 * Once that's done, we'll create two tables to display the info to the user. One will be sorted by
 * alphabet, the other will be sorted by frequency in descending order.
 */

function process(text) {

    // This map will store the data - it will map each character from A - Z, with the number of times
    // that character appears. For example, char_freq_map["a"] will give us the number of times "a"
    // or "A" appears in the text (we're ignoring case).
    var char_freq_map = {};

    var i = -1;
    var character = "";
    var keysArray; // an array of the keys
    var contents = "";

    // Populate char_freq_map with character frequency information
    for (i = 0; i < text.length; i++) {
        character = text.charAt(i);

        // Skip punctuation and numbers to deal with just the chars from the alphabet
        if ((character >= 'a' && character <= 'z') || (character >= 'A' && character <= 'Z')) {
            character = character.toLowerCase(); // Ignore case

            if (char_freq_map[character] === undefined) {
                char_freq_map[character] = 1;
            }
            else {
                char_freq_map[character]++;

            }

            // TODO step 2: if char_freq_map already contains an entry for the given character,
            // add one to that entry. Otherwise, add a new entry for that character with the
            // initial value of 1.

        }
    }

    // Obtain arrays of all characters in char_freq_map, sorted both alphabetically (alphaSortedKeys)
    // and by frequency (freqSortedKeys).
    var alphaSortedKeys = Object.keys(char_freq_map).sort();
    var freqSortedKeys = Object.keys(char_freq_map).sort(function (a, b) {
        return char_freq_map[b] - char_freq_map[a];
    });

    var sortTable = $("#table-alphasort");
    var freqTable = $("#table-freqsort");
    createTable(sortTable, char_freq_map, alphaSortedKeys);
    createTable(freqTable, char_freq_map, freqSortedKeys);

    // TODO step 4: Call the createTable function twice, to populate the tables.
}

/**
 * When the document is ready, we'll add an event handler to the button which will initiate processing.
 */
$(document).ready(function () {
    $("#analyzebutton").click(function () {
        var text = $("#thetext").val();
        process(text);
    });
    // TODO step 1: add an event handler to the analyze button, so that when the button is clicked, the process() function
    // will be called, with the text area's text passed in as an argument.

});
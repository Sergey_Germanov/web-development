"use strict";

// TODO Your code here.
$(function () {
    var human = $("#figure");
    human.animate({ left: "38px", top: "100px" });
    human.animate({ left: "38px", top: "34px" });
    human.animate({ left: "100px", top: "34px" });
    human.animate({ left: "100px", top: "162px" });
    human.animate({ left: "37px", top: "162px" });
    human.animate({ left: "37px", top: "225px" });
    human.animate({ left: "165px", top: "225px" });
    human.animate({ left: "165px", top: "100px" });
    human.animate({ left: "222px", top: "100px" });
    human.animate({ left: "222px", top: "225px" });
    human.animate({ left: "320px", top: "225px" });

    var buggieMan = $("#monster");
    buggieMan.delay(2000).animate({ left: "25px", top: "100px" });
    buggieMan.animate({ left: "25px", top: "34px" });
    buggieMan.animate({ left: "82px", top: "34px" }, "fast");
    buggieMan.animate({ left: "82px", top: "162px" }, "fast");
    buggieMan.animate({ left: "22px", top: "162px" }, "fast");
    buggieMan.animate({ left: "22px", top: "225px" }, "fast");
    buggieMan.animate({ left: "147px", top: "225px" }, "fast");
    buggieMan.animate({ left: "147px", top: "100px" }, "fast");
    buggieMan.animate({ left: "204px", top: "100px" }, "fast");
    buggieMan.animate({ left: "204px", top: "225px" }, "fast");
    buggieMan.animate({ left: "250px", top: "225px" }, "fast");

});


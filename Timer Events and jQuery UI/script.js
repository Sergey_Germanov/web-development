/* Develop your answer to Question 1 here */
"use strict";

var current_banner_num = 1;

$(document).ready(function () {

	$("#banner-base").css("visibility", "hidden"); // Use hidden to keep width and height of div same as image
	$("#banner-" + current_banner_num).show();

	// Set up the #next div as a jQuery UI button
	var next_button = $("#next").button();
	var stop_button = $("#stop").button();
	var checkRun = false;
	var StopSlides;

	var speedControl = 3000;
	$("#size-control").slider({
		orientation: "horizontal",
		min: 1000,
		max: 6000,
		value: 3000,
		slide: function (event, ui) {

			speedControl = ui.value;

			if (checkRun == true) {
				clearInterval(StopSlides);
				StopSlides = setInterval(showSlide, ui.value);
			}


		}
	});

	next_button.click(function () {
		if (checkRun == false) {
			StopSlides = setInterval(showSlide, speedControl);
			checkRun = true;
		}

	});


	stop_button.click(function () {
		clearInterval(StopSlides);
		checkRun = false;
	});



	function showSlide() {

		// Hide the current banner
		$("#banner-" + current_banner_num).fadeOut(speedControl);
		// Update which banner is "current"

		if (current_banner_num == 4) {
			current_banner_num = 1;
		}
		else {
			current_banner_num++;
		}

		// Show the current banner
		$("#banner-" + current_banner_num).fadeIn(speedControl);
	};

});


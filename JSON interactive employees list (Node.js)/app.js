var express = require('express');

var fs = require('fs');

var app = express();

app.set('port', process.env.PORT || 3000);


var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));


var cookieParser = require('cookie-parser');
app.use(cookieParser());

app.get('/', function (req, res) {

    fs.readFile(__dirname + "/employees.json", function (err, data) {

        employeesData = JSON.parse(data);
        res.render('home', { employees: employeesData });

    });

});


app.get('/ascending', function (req, res) {

    employeesData.sort(function (a, b) {
        return a.lname < b.lname;
    });

    res.render("home", { employees: employeesData });
});

app.get('/descending', function (req, res) {


    employeesData.sort(function (a, b) {
        return a.lname > b.lname;
    });

    res.render("home", { employees: employeesData });
});

app.get('/remove', function (req, res) {

    var Data = req.query.EmployeesSelect;

    if (!Array.isArray(Data)) {
        Data = [Data];
    }

    var newEmployees = [];
    for (var i = 0; i < employeesData.length; i++) {
        if (Data.indexOf(employeesData[i]["id"].toString()) < 0) {

            newEmployees.push(employeesData[i]);
        }
    }

    var data = JSON.stringify(newEmployees);
    fs.writeFileSync("employees.json", data);

    res.redirect('/');

});


app.get('/add', function (req, res) {

    res.render("add");
});

app.post('/add', function (req, res) {

    var max;
    var Data = req.body;
    var Cookies = req.cookies;


    var cool = [];
    var i;
    for (var property in Cookies) {
        x = Cookies[property];
        cool.push(x);
    }
    function checkArray() {
        for (var i = 0; i <= cool.length; i++) {
            if (cool[i] === "") {
                return true;
            }

        } return false;
    }
    checkArray = checkArray(cool);
    if (checkArray === true) {

        res.redirect('/add');

    }
    if (checkArray !== true) {


        fs.readFile(__dirname + "/employees.json", function (err, data) {

            var employeesData = JSON.parse(data);


            var ids = [];
            var max;
            var arrayLength = employeesData.length;
            for (var i = 0; i < employeesData.length; i++) {
                ids.push(employeesData[i].id)
            }

            var max = ids.reduce(function (a, b) {
                return Math.max(a, b);

            });

            if (arrayLength == max) {
                var newNum = "id";
                var newVal = arrayLength + 1;
                Data[newNum] = newVal;
                var newVal1 = newVal - 1
                employeesData[newVal1] = Data;
            }
            else {
                var newNum = "id";
                var newVal = arrayLength;
                if (newVal < max) {
                    newVal = max + 1;
                }
                Data[newNum] = newVal;
                employeesData[arrayLength] = Data;
            }

            var addEmployee = JSON.stringify(employeesData);
            fs.writeFileSync("employees.json", addEmployee);



            res.clearCookie("lname", { path: '/add' });
            res.clearCookie("fname", { path: '/add' });
            res.clearCookie("email", { path: '/add' });
            res.clearCookie("ph", { path: '/add' });
            res.clearCookie("address", { path: '/add' });
            res.clearCookie("title", { path: '/add' });


            res.redirect('/');
        })
    }



});




app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});
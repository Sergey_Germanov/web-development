"use strict";

window.onload = function () {
    var pages = ["page0", "page1", "page2", "page3", "page4", "page5", "page6", "page7", "page8", "page9", "page10", "page11"]

    var i;
    for (i = 0; i < pages.length; i++) {
        var startAnimate = document.getElementById(pages[i]);
        startAnimate.style.animationDelay = "initial";
        startAnimate.onclick = animeNext;
    };

};
var animeNext = function () {
    this.classList.add("pageAnimation");
    if (this.nextElementSibling) {
        this.addEventListener("animationend", function () {
            this.nextElementSibling.style.zIndex = 0;
        });
    };
};